package cl.guille.usuarios.fixtures;

import cl.guille.usuarios.daos.models.UsuarioModel;
import cl.guille.usuarios.utils.JsonUtil;

import java.util.List;
import java.util.Map;

public class FixtureUsuario {

    /*
     */
    public static final Map<String, List<UsuarioModel>> findAllRs;
    /*
     */
    public static final Map<String, UsuarioModel> findByIdRs;
    /*
     */
    public static final Map<String, List<UsuarioModel>> findByEmailRs;

    static {
        findAllRs = JsonUtil.toMapObject(
                Fixture.readFileFromResources("usuarioRepository/findAllRs.json"),
                String.class, List.class);
        findByIdRs = JsonUtil.toMapObject(
                Fixture.readFileFromResources("usuarioRepository/findByIdRs.json"),
                String.class, UsuarioModel.class);
        findByEmailRs = JsonUtil.toMapObject(
                Fixture.readFileFromResources("usuarioRepository/findByEmailRs.json"),
                String.class, List.class);
    }
}
