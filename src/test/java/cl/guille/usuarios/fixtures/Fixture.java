package cl.guille.usuarios.fixtures;

import cl.guille.usuarios.utils.JsonUtil;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static java.util.Objects.isNull;

public class Fixture {

    private static final String PATH = "src/test/resources/jsons/";

    public static String readFileFromResources(String fileName) {
        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(new FileInputStream(PATH + fileName), StandardCharsets.UTF_8))) {
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
                sb.append('\n');
            }
            return sb.toString();
        } catch (Exception e) {
            return null;
        }
    }

    public static <T> T getObjectFromMap(String metodo, int currentRepetition, Map<String, T> mapa) {
        String key = mapa.keySet().stream().filter(k -> k.equalsIgnoreCase(metodo + "-" + currentRepetition))
                .findFirst().orElse(metodo + "-OK");
        return mapa.get(key);
    }

    public static <T, K> List<K> getListFromMap(String metodo, int currentRepetition, Map<String, T> mapa, Class<K> clase) {
        T object = getObjectFromMap(metodo, currentRepetition, mapa);
        return JsonUtil.convertToListObject(object, clase);
    }

    public static <T> Optional<T> getOptionalFromMap(String metodo, int currentRepetition, Map<String, T> mapa) {
        T object = getObjectFromMap(metodo, currentRepetition, mapa);
        return isNull(object) ? Optional.empty() : Optional.of(object);
    }

    public static <T> String getJsonFromMap(String metodo, int currentRepetition, Map<String, T> mapa) {
        T object = getObjectFromMap(metodo, currentRepetition, mapa);
        return JsonUtil.toJson(object);
    }
}
