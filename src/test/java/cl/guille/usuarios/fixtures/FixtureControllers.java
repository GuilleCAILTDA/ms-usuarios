package cl.guille.usuarios.fixtures;

import cl.guille.usuarios.dtos.UsuarioDTO;
import cl.guille.usuarios.utils.JsonUtil;

import java.util.Map;

public class FixtureControllers {
    /*
     */
    public static final Map<String, UsuarioDTO> saveRq;
    /*
     */
    public static final Map<String, UsuarioDTO> updateRq;

    static {
        saveRq = JsonUtil.toMapObject(
                Fixture.readFileFromResources("controllers/saveRq.json"),
                String.class, UsuarioDTO.class);
        updateRq = JsonUtil.toMapObject(
                Fixture.readFileFromResources("controllers/updateRq.json"),
                String.class, UsuarioDTO.class);
    }
}
