package cl.guille.usuarios.controllers;

import cl.guille.usuarios.daos.models.UsuarioModel;
import cl.guille.usuarios.daos.repositories.UsuarioRepository;
import cl.guille.usuarios.enums.CatalogoEnum;
import cl.guille.usuarios.fixtures.Fixture;
import cl.guille.usuarios.fixtures.FixtureControllers;
import cl.guille.usuarios.fixtures.FixtureUsuario;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.RepetitionInfo;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.List.of;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UsuarioControllerTest {

    private static final String BASE_ENDPOINT = "/usuario";

    @Autowired
    private MockMvc mvc;
    @Autowired
    private WebApplicationContext context;
    @MockBean
    private UsuarioRepository usuarioRepository;

    @BeforeEach
    public void setup() throws Exception {
        this.mvc = MockMvcBuilders.webAppContextSetup(context).build();
    }

    @RepeatedTest(1)
    void findAll(RepetitionInfo repetitionInfo) throws Exception {
        int currentRepetition = repetitionInfo.getCurrentRepetition();
        List<ResultMatcher> resultMatcherList = resultFindAll(currentRepetition);

        ResultActions result = mvc.perform(get(BASE_ENDPOINT)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, ""));
        result.andDo(print())
                // STATUS
                .andExpect(resultMatcherList.get(0))
                // HEADER
                .andExpect(resultMatcherList.get(1))
                // BODY
                .andExpect(resultMatcherList.get(2));
    }

    private List<ResultMatcher> resultFindAll(int currentRepetition) throws Exception {
        String metodo = "findAll";
        when(usuarioRepository.findAll())
                .thenReturn(Fixture.getListFromMap(metodo, currentRepetition, FixtureUsuario.findAllRs, UsuarioModel.class));
        Map<Integer, CatalogoEnum> errores = new HashMap<>();
        List<ResultMatcher> resultMatcherList;
        switch (currentRepetition) {
            case 1: /* flujo exitoso */
                resultMatcherList = of(status().is(HttpStatus.OK.value()),
                        header().exists(HttpHeaders.AUTHORIZATION),
                        jsonPath("$.[0].id").value(1));
                break;
            default:
                resultMatcherList = resultMatcherList = of(status().is(errores.get(currentRepetition).getStatus()),
                        header().doesNotExist(HttpHeaders.AUTHORIZATION),
                        jsonPath("$.codigo").value(errores.get(currentRepetition).getCodigo()));
                break;
        }
        return resultMatcherList;
    }

    @RepeatedTest(1)
    void findById(RepetitionInfo repetitionInfo) throws Exception {
        int currentRepetition = repetitionInfo.getCurrentRepetition();
        List<ResultMatcher> resultMatcherList = resultFindById(currentRepetition);

        ResultActions result = mvc.perform(get(BASE_ENDPOINT + "/1")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, ""));
        result.andDo(print())
                // STATUS
                .andExpect(resultMatcherList.get(0))
                // HEADER
                .andExpect(resultMatcherList.get(1))
                // BODY
                .andExpect(resultMatcherList.get(2));
    }

    private List<ResultMatcher> resultFindById(int currentRepetition) throws Exception {
        String metodo = "findById";
        when(usuarioRepository.findById(any()))
                .thenReturn(Fixture.getOptionalFromMap(metodo, currentRepetition, FixtureUsuario.findByIdRs));
        Map<Integer, CatalogoEnum> errores = new HashMap<>();
        List<ResultMatcher> resultMatcherList;
        switch (currentRepetition) {
            case 1: /* flujo exitoso */
                resultMatcherList = of(status().is(HttpStatus.OK.value()),
                        header().exists(HttpHeaders.AUTHORIZATION),
                        jsonPath("$.id").value(1));
                break;
            default:
                resultMatcherList = resultMatcherList = of(status().is(errores.get(currentRepetition).getStatus()),
                        header().doesNotExist(HttpHeaders.AUTHORIZATION),
                        jsonPath("$.codigo").value(errores.get(currentRepetition).getCodigo()));
                break;
        }
        return resultMatcherList;
    }

    @RepeatedTest(5)
    void save(RepetitionInfo repetitionInfo) throws Exception {
        String metodo = "save";
        int currentRepetition = repetitionInfo.getCurrentRepetition();
        List<ResultMatcher> resultMatcherList = resultSave(currentRepetition);

        ResultActions result = mvc.perform(post(BASE_ENDPOINT)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(Fixture.getJsonFromMap(metodo, currentRepetition, FixtureControllers.saveRq))
                .header(HttpHeaders.AUTHORIZATION, ""));
        result.andDo(print())
                // STATUS
                .andExpect(resultMatcherList.get(0))
                // HEADER
                .andExpect(resultMatcherList.get(1))
                // BODY
                .andExpect(resultMatcherList.get(2));
    }

    private List<ResultMatcher> resultSave(int currentRepetition) throws Exception {
        String metodo = "save";
        when(usuarioRepository.findByEmail(any()))
                .thenReturn(Fixture.getListFromMap(metodo, currentRepetition, FixtureUsuario.findByEmailRs, UsuarioModel.class));
        when(usuarioRepository.saveAndFlush(any())).thenReturn(UsuarioModel.builder().phones(List.of()).build());
        Map<Integer, CatalogoEnum> errores = new HashMap<>();
        errores.put(2, CatalogoEnum.DEFAULT_ERROR);
        errores.put(3, CatalogoEnum.ERROR_GUARDAR_CORREO);
        errores.put(4, CatalogoEnum.BAD_REQUEST);
        errores.put(5, CatalogoEnum.BAD_REQUEST);
        List<ResultMatcher> resultMatcherList;
        switch (currentRepetition) {
            case 1: /* flujo exitoso */
                resultMatcherList = of(status().is(HttpStatus.NO_CONTENT.value()),
                        header().exists(HttpHeaders.AUTHORIZATION),
                        jsonPath("$").doesNotExist());
                break;
            case 2:
                when(usuarioRepository.saveAndFlush(any())).thenThrow(NullPointerException.class);
                resultMatcherList = of(status().is(errores.get(currentRepetition).getStatus()),
                        header().doesNotExist(HttpHeaders.AUTHORIZATION),
                        jsonPath("$.codigo").value(errores.get(currentRepetition).getCodigo()));
                break;
            default:
                resultMatcherList = of(status().is(errores.get(currentRepetition).getStatus()),
                        header().doesNotExist(HttpHeaders.AUTHORIZATION),
                        jsonPath("$.codigo").value(errores.get(currentRepetition).getCodigo()));
                break;
        }
        return resultMatcherList;
    }

    @RepeatedTest(3)
    void update(RepetitionInfo repetitionInfo) throws Exception {
        String metodo = "update";
        int currentRepetition = repetitionInfo.getCurrentRepetition();
        List<ResultMatcher> resultMatcherList = resultUpdate(currentRepetition);

        ResultActions result = mvc.perform(put(BASE_ENDPOINT)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(Fixture.getJsonFromMap(metodo, currentRepetition, FixtureControllers.updateRq))
                .header(HttpHeaders.AUTHORIZATION, ""));
        result.andDo(print())
                // STATUS
                .andExpect(resultMatcherList.get(0))
                // HEADER
                .andExpect(resultMatcherList.get(1))
                // BODY
                .andExpect(resultMatcherList.get(2));
    }

    private List<ResultMatcher> resultUpdate(int currentRepetition) throws Exception {
        String metodo = "update";
        when(usuarioRepository.findById(any()))
                .thenReturn(Fixture.getOptionalFromMap(metodo, currentRepetition, FixtureUsuario.findByIdRs));
        when(usuarioRepository.saveAndFlush(any())).thenReturn(UsuarioModel.builder().phones(List.of()).build());
        Map<Integer, CatalogoEnum> errores = new HashMap<>();
        errores.put(3, CatalogoEnum.ERROR_ACTUALIZAR_NO_EXISTE);
        List<ResultMatcher> resultMatcherList;
        switch (currentRepetition) {
            case 1: /* flujo exitoso */
            case 2:
                resultMatcherList = of(status().is(HttpStatus.NO_CONTENT.value()),
                        header().exists(HttpHeaders.AUTHORIZATION),
                        jsonPath("$").doesNotExist());
                break;
            default:
                resultMatcherList = resultMatcherList = of(status().is(errores.get(currentRepetition).getStatus()),
                        header().doesNotExist(HttpHeaders.AUTHORIZATION),
                        jsonPath("$.codigo").value(errores.get(currentRepetition).getCodigo()));
                break;
        }
        return resultMatcherList;
    }

    @RepeatedTest(1)
    void deleteAll(RepetitionInfo repetitionInfo) throws Exception {
        int currentRepetition = repetitionInfo.getCurrentRepetition();
        List<ResultMatcher> resultMatcherList = resultDeleteAll(currentRepetition);

        ResultActions result = mvc.perform(delete(BASE_ENDPOINT)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, ""));
        result.andDo(print())
                // STATUS
                .andExpect(resultMatcherList.get(0))
                // HEADER
                .andExpect(resultMatcherList.get(1))
                // BODY
                .andExpect(resultMatcherList.get(2));
    }

    private List<ResultMatcher> resultDeleteAll(int currentRepetition) throws Exception {
        doNothing().when(usuarioRepository).deleteAll();
        Map<Integer, CatalogoEnum> errores = new HashMap<>();
        List<ResultMatcher> resultMatcherList;
        switch (currentRepetition) {
            case 1: /* flujo exitoso */
                resultMatcherList = of(status().is(HttpStatus.NO_CONTENT.value()),
                        header().exists(HttpHeaders.AUTHORIZATION),
                        jsonPath("$").doesNotExist());
                break;
            default:
                resultMatcherList = resultMatcherList = of(status().is(errores.get(currentRepetition).getStatus()),
                        header().doesNotExist(HttpHeaders.AUTHORIZATION),
                        jsonPath("$.codigo").value(errores.get(currentRepetition).getCodigo()));
                break;
        }
        return resultMatcherList;
    }

    @RepeatedTest(1)
    void deleteById(RepetitionInfo repetitionInfo) throws Exception {
        int currentRepetition = repetitionInfo.getCurrentRepetition();
        List<ResultMatcher> resultMatcherList = resultDeleteById(currentRepetition);

        ResultActions result = mvc.perform(delete(BASE_ENDPOINT + "/1")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, ""));
        result.andDo(print())
                // STATUS
                .andExpect(resultMatcherList.get(0))
                // HEADER
                .andExpect(resultMatcherList.get(1))
                // BODY
                .andExpect(resultMatcherList.get(2));
    }

    private List<ResultMatcher> resultDeleteById(int currentRepetition) throws Exception {
        doNothing().when(usuarioRepository).deleteById(any());
        Map<Integer, CatalogoEnum> errores = new HashMap<>();
        List<ResultMatcher> resultMatcherList;
        switch (currentRepetition) {
            case 1: /* flujo exitoso */
                resultMatcherList = of(status().is(HttpStatus.NO_CONTENT.value()),
                        header().exists(HttpHeaders.AUTHORIZATION),
                        jsonPath("$").doesNotExist());
                break;
            default:
                resultMatcherList = resultMatcherList = of(status().is(errores.get(currentRepetition).getStatus()),
                        header().doesNotExist(HttpHeaders.AUTHORIZATION),
                        jsonPath("$.codigo").value(errores.get(currentRepetition).getCodigo()));
                break;
        }
        return resultMatcherList;
    }
}
