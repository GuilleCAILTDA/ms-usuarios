package cl.guille.usuarios.enums;

public enum LogEstadoEnum {
    INICIO,
    FIN,
    WARN,
    INFO,
    ERROR,
    VALID,
    INVALID,
    REQUEST,
    RESPONSE,
    ;
}
