package cl.guille.usuarios.enums;

import cl.guille.usuarios.exceptions.IError;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import org.springframework.http.HttpStatus;

import java.util.HashMap;
import java.util.Map;

@AllArgsConstructor
@Getter
@ToString
public enum CatalogoEnum implements IError {

    DEFAULT_ERROR("MSUSUARIO-001", "Error interno servidor", HttpStatus.INTERNAL_SERVER_ERROR.value()),
    BAD_REQUEST("MSUSUARIO-002", "Par\u00E1metros de entrada no v\u00E1lidos", HttpStatus.BAD_REQUEST.value()),
    API_NOT_FOUND("MSUSUARIO-003", "Api no existente", HttpStatus.INTERNAL_SERVER_ERROR.value()),
    ERROR_BD("MSUSUARIO-004", "Error en base de datos", HttpStatus.INTERNAL_SERVER_ERROR.value()),
    ERROR_GUARDAR_CORREO("MSUSUARIO-010", "El correo se encuentra registrado", HttpStatus.INTERNAL_SERVER_ERROR.value()),
    ERROR_ACTUALIZAR_NO_EXISTE("MSUSUARIO-011", "No existe el usuario a actualizar", HttpStatus.INTERNAL_SERVER_ERROR.value()),
    ;

    private final String codigo;
    private final String mensaje;
    private final int status;

    private static Map<String, CatalogoEnum> map = new HashMap<>();

    static {
        for (CatalogoEnum catalogoEnum : CatalogoEnum.values()) {
            map.put(catalogoEnum.codigo, catalogoEnum);
        }
    }

    public static CatalogoEnum getCodigoValueOf(String codigo) {
        return map.getOrDefault(codigo, DEFAULT_ERROR);
    }
}