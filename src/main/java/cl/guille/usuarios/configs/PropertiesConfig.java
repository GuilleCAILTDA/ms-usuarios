package cl.guille.usuarios.configs;

import cl.guille.usuarios.domains.swagger.SwaggerProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySources;

@PropertySources({
})
@Configuration
public class PropertiesConfig {

    @Bean
    @ConfigurationProperties(prefix = "swagger")
    public SwaggerProperties swaggerProperties() {
        return new SwaggerProperties();
    }
}
