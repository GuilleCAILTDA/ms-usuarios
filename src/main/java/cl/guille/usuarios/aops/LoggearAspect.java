package cl.guille.usuarios.aops;

import cl.guille.usuarios.utils.JsonUtil;
import cl.guille.usuarios.utils.LogUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Aspect
@Component
public class LoggearAspect {

    private Map<String, Logger> loggers = new HashMap<>();

    @Around("@annotation(loggear)")
    public Object loggear(final ProceedingJoinPoint pjp, Loggear loggear) throws Throwable {
        Logger logger = getLogger(loggear);
        String metodo = pjp.getSignature().getName();
        Object[] args = getArgs(pjp);
        LogUtil.logInfo(logger, metodo, loggear.anterior().toString(), JsonUtil.toJson(args));
        Object response = pjp.proceed();
        LogUtil.logInfo(logger, metodo, loggear.posterior().toString(), JsonUtil.toJson(response));
        return response;
    }

    private Logger getLogger(Loggear loggear) {
        if (loggers.get(loggear.clase().getName()) == null) {
            loggers.put(loggear.clase().getName(), LogManager.getLogger(loggear.clase()));
        }
        return loggers.get(loggear.clase().getName());
    }

    private Object[] getArgs(ProceedingJoinPoint pjp) {
        return pjp.getArgs().length > 0 ? pjp.getArgs() : null;
    }
}
