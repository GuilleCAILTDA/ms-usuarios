package cl.guille.usuarios.aops;

import cl.guille.usuarios.enums.LogEstadoEnum;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.ANNOTATION_TYPE})
public @interface Loggear {
    Class clase();

    LogEstadoEnum anterior();

    LogEstadoEnum posterior();
}
