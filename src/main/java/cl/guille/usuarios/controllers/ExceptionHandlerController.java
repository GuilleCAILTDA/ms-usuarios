package cl.guille.usuarios.controllers;

import cl.guille.usuarios.enums.CatalogoEnum;
import cl.guille.usuarios.exceptions.Error;
import cl.guille.usuarios.exceptions.IError;
import cl.guille.usuarios.exceptions.MSException;
import cl.guille.usuarios.utils.JsonUtil;
import cl.guille.usuarios.utils.LogUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;

import static cl.guille.usuarios.enums.LogEstadoEnum.ERROR;

@ControllerAdvice
public class ExceptionHandlerController {
    private static final Logger LOGGER = LogManager.getLogger(ExceptionHandlerController.class);

    @ExceptionHandler({Exception.class})
    public ResponseEntity<Error> handleException(HttpServletRequest req, Exception e) {
        Error error = generateError(CatalogoEnum.DEFAULT_ERROR, e.getLocalizedMessage(), e.getClass().getSimpleName());
        LogUtil.logError(LOGGER, "handleException", ERROR.toString(), JsonUtil.toJson(error), e);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
    }

    @ExceptionHandler({MethodArgumentNotValidException.class})
    public ResponseEntity<Error> handleMethodArgumentNotValidException(HttpServletRequest req, Exception e) {
        Error error = generateError(CatalogoEnum.BAD_REQUEST, e.getLocalizedMessage(), e.getClass().getSimpleName());
        LogUtil.logError(LOGGER, "handleMethodArgumentNotValidException", ERROR.toString(), JsonUtil.toJson(error), e);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
    }

    @ExceptionHandler({DataAccessException.class})
    public ResponseEntity<Error> handleDataAccessException(HttpServletRequest req, Exception e) {
        Error error = generateError(CatalogoEnum.ERROR_BD, e.getLocalizedMessage(), e.getClass().getSimpleName());
        LogUtil.logError(LOGGER, "handleDataAccessException", ERROR.toString(), JsonUtil.toJson(error), e);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
    }

    @ExceptionHandler({MSException.class})
    public ResponseEntity<Error> handleMSException(HttpServletRequest req, MSException e) {
        boolean causa = e.getCause() != null;
        CatalogoEnum catalogo = (CatalogoEnum) e.getError();
        Error error = generateError(e.getError(),
                causa ? e.getCause().getLocalizedMessage() : catalogo.getMensaje(),
                causa ? e.getCause().getClass().getSimpleName() : catalogo.name());
        LogUtil.logError(LOGGER, "handleMSException", ERROR.toString(), JsonUtil.toJson(error), e);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
    }

    private static Error generateError(IError ierror, String traza, String causa) {
        return Error.builder()
                .codigo(ierror.getCodigo())
                .mensaje(ierror.getMensaje())
                .traza(traza)
                .causa(causa)
                .build();
    }
}