package cl.guille.usuarios.controllers;

import cl.guille.usuarios.dtos.UsuarioDTO;
import cl.guille.usuarios.services.UsuarioService;
import cl.guille.usuarios.utils.HeadersUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@Api(value = "Gestion de data de un usuario", tags = {"usuario"})
@Controller
@RequestMapping("/usuario")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @ApiOperation(
            value = "Obtiene todos los usuarios",
            notes = "Obtiene todos los usuarios",
            produces = "ResponseEntity<List<UsuarioDTO>>")
    @ApiResponses({
            @ApiResponse(code = 400, message = "Solicitud incorrecta"),
            @ApiResponse(code = 401, message = "No autorizado"),
            @ApiResponse(code = 403, message = "No permitido"),
            @ApiResponse(code = 500, message = "Error general"),
            @ApiResponse(code = 200, message = "Informacion obtenida")
    })
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<UsuarioDTO>> findAll(
            HttpServletRequest request,
            @RequestHeader(HttpHeaders.AUTHORIZATION) String jwt) {
        return ResponseEntity.ok()
                .headers(HeadersUtil.getGenericHeaders(jwt))
                .body(usuarioService.findAll());
    }

    @ApiOperation(
            value = "Obtiene un usuario",
            notes = "Obtiene un usuario",
            produces = "ResponseEntity<UsuarioDTO>")
    @ApiResponses({
            @ApiResponse(code = 400, message = "Solicitud incorrecta"),
            @ApiResponse(code = 401, message = "No autorizado"),
            @ApiResponse(code = 403, message = "No permitido"),
            @ApiResponse(code = 500, message = "Error general"),
            @ApiResponse(code = 200, message = "Informacion obtenida")
    })
    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UsuarioDTO> findById(
            HttpServletRequest request,
            @RequestHeader(HttpHeaders.AUTHORIZATION) String jwt,
            @PathVariable Integer id) {
        return ResponseEntity.ok()
                .headers(HeadersUtil.getGenericHeaders(jwt))
                .body(usuarioService.findById(id));
    }

    @ApiOperation(
            value = "Almacena un usuario",
            notes = "Almacena un usuario",
            produces = "ResponseEntity<Void>")
    @ApiResponses({
            @ApiResponse(code = 400, message = "Solicitud incorrecta"),
            @ApiResponse(code = 401, message = "No autorizado"),
            @ApiResponse(code = 403, message = "No permitido"),
            @ApiResponse(code = 500, message = "Error general"),
            @ApiResponse(code = 204, message = "Informacion guardada")
    })
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> save(
            HttpServletRequest request,
            @RequestHeader(HttpHeaders.AUTHORIZATION) String jwt,
            @RequestBody @Valid UsuarioDTO usuario) {
        usuarioService.save(usuario);
        return ResponseEntity.noContent()
                .headers(HeadersUtil.getGenericHeaders(jwt)).build();
    }

    @ApiOperation(
            value = "Actualiza un usuario",
            notes = "Actualiza un usuario",
            produces = "ResponseEntity<Void>")
    @ApiResponses({
            @ApiResponse(code = 400, message = "Solicitud incorrecta"),
            @ApiResponse(code = 401, message = "No autorizado"),
            @ApiResponse(code = 403, message = "No permitido"),
            @ApiResponse(code = 500, message = "Error general"),
            @ApiResponse(code = 204, message = "Informacion guardada")
    })
    @PutMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> update(
            HttpServletRequest request,
            @RequestHeader(HttpHeaders.AUTHORIZATION) String jwt,
            @PathVariable Integer id,
            @RequestBody @Valid UsuarioDTO usuario) {
        usuario.setId(id);
        usuarioService.update(usuario);
        return ResponseEntity.noContent()
                .headers(HeadersUtil.getGenericHeaders(jwt)).build();
    }

    @ApiOperation(
            value = "Elimina todos los usuarios",
            notes = "Elimina todos los usuarios",
            produces = "ResponseEntity<Void>")
    @ApiResponses({
            @ApiResponse(code = 400, message = "Solicitud incorrecta"),
            @ApiResponse(code = 401, message = "No autorizado"),
            @ApiResponse(code = 403, message = "No permitido"),
            @ApiResponse(code = 500, message = "Error general"),
            @ApiResponse(code = 204, message = "Informacion eliminada")
    })
    @DeleteMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> deleteAll(
            HttpServletRequest request,
            @RequestHeader(HttpHeaders.AUTHORIZATION) String jwt) {
        usuarioService.deleteAll();
        return ResponseEntity.noContent()
                .headers(HeadersUtil.getGenericHeaders(jwt)).build();
    }

    @ApiOperation(
            value = "Elimina un usuario",
            notes = "Elimina un usuario",
            produces = "ResponseEntity<Void>")
    @ApiResponses({
            @ApiResponse(code = 400, message = "Solicitud incorrecta"),
            @ApiResponse(code = 401, message = "No autorizado"),
            @ApiResponse(code = 403, message = "No permitido"),
            @ApiResponse(code = 500, message = "Error general"),
            @ApiResponse(code = 204, message = "Informacion eliminada")
    })
    @DeleteMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> deleteById(
            HttpServletRequest request,
            @RequestHeader(HttpHeaders.AUTHORIZATION) String jwt,
            @PathVariable Integer id) {
        usuarioService.deleteById(id);
        return ResponseEntity.noContent()
                .headers(HeadersUtil.getGenericHeaders(jwt)).build();
    }
}
