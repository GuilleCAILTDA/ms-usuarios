package cl.guille.usuarios.exceptions;

public interface IError {
    String getCodigo();

    String getMensaje();

    default int getStatus() {
        return 500;
    }

    ;
}
