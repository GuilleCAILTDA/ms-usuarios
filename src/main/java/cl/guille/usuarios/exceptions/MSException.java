package cl.guille.usuarios.exceptions;

public class MSException extends RuntimeException {
    private final transient IError error;

    public MSException(IError error) {
        super(error.getMensaje());
        this.error = error;
    }

    public MSException(IError error, Throwable ex) {
        super(error.getMensaje(), ex);
        this.error = error;
    }

    public IError getError() {
        return this.error;
    }
}
