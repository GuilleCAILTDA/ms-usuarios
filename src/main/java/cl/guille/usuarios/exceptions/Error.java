package cl.guille.usuarios.exceptions;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@ToString
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Error {

    private String codigo;
    private String mensaje;
    private String traza;
    private String causa;
    private int httpStatus;

}
