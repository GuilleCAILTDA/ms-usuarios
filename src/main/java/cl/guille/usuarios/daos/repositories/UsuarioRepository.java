package cl.guille.usuarios.daos.repositories;

import cl.guille.usuarios.aops.Loggear;
import cl.guille.usuarios.daos.models.UsuarioModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

import static cl.guille.usuarios.enums.LogEstadoEnum.REQUEST;
import static cl.guille.usuarios.enums.LogEstadoEnum.RESPONSE;

@Repository
public interface UsuarioRepository extends JpaRepository<UsuarioModel, Integer> {

    List<UsuarioModel> findByEmail(String email);

    @Loggear(clase = UsuarioRepository.class, anterior = REQUEST, posterior = RESPONSE)
    UsuarioModel saveAndFlush(UsuarioModel model);


}
