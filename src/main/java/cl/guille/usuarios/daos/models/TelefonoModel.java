package cl.guille.usuarios.daos.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class TelefonoModel {

    @Id
    @GeneratedValue
    private Integer id;
    private String number;
    private String cityCode;
    private String countryCode;
    private Date created;
    private Date modified;
}
