package cl.guille.usuarios.services;

import cl.guille.usuarios.aops.Loggear;
import cl.guille.usuarios.converters.TelefonoConverter;
import cl.guille.usuarios.converters.UsuarioConverter;
import cl.guille.usuarios.daos.models.TelefonoModel;
import cl.guille.usuarios.daos.models.UsuarioModel;
import cl.guille.usuarios.daos.repositories.UsuarioRepository;
import cl.guille.usuarios.dtos.UsuarioDTO;
import cl.guille.usuarios.enums.CatalogoEnum;
import cl.guille.usuarios.exceptions.MSException;
import cl.guille.usuarios.utils.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Optional;

import static cl.guille.usuarios.enums.LogEstadoEnum.FIN;
import static cl.guille.usuarios.enums.LogEstadoEnum.INICIO;
import static java.util.Objects.requireNonNullElse;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;
    @Autowired
    private UsuarioConverter usuarioConverter;
    @Autowired
    private TelefonoConverter telefonoConverter;

    @Loggear(clase = UsuarioService.class, anterior = INICIO, posterior = FIN)
    public List<UsuarioDTO> findAll() {
        List<UsuarioModel> models = usuarioRepository.findAll();
        List<UsuarioDTO> response = usuarioConverter.toDTOs(requireNonNullElse(models, List.of()));
        return response;
    }

    @Loggear(clase = UsuarioService.class, anterior = INICIO, posterior = FIN)
    public UsuarioDTO findById(Integer id) {
        UsuarioDTO response = null;
        Optional<UsuarioModel> model = usuarioRepository.findById(id);
        if (model.isPresent()) {
            response = usuarioConverter.convert(model.get());
        }
        return response;
    }

    @Loggear(clase = UsuarioService.class, anterior = INICIO, posterior = FIN)
    public void save(UsuarioDTO dto) {
        List<UsuarioModel> correoRegistrado = usuarioRepository.findByEmail(dto.getEmail());
        if (!CollectionUtils.isEmpty(correoRegistrado)) {
            throw new MSException(CatalogoEnum.ERROR_GUARDAR_CORREO);
        }
        UsuarioModel model = usuarioConverter.convert(dto);
        model.getPhones().forEach(t -> t.setCreated(DateUtil.fechaStgo()));
        model.setCreated(DateUtil.fechaStgo());
        model.setLastLogin(DateUtil.fechaStgo());
        model.setActive(true);
        usuarioRepository.saveAndFlush(model);
    }

    @Loggear(clase = UsuarioService.class, anterior = INICIO, posterior = FIN)
    public void update(UsuarioDTO dto) {
        Optional<UsuarioModel> optional = usuarioRepository.findById(dto.getId());
        if (optional.isPresent()) {
            UsuarioModel model = optional.get();
            model.setName(dto.getName());
            model.setEmail(dto.getEmail());
            model.setPassword(dto.getPassword());
            List<TelefonoModel> models = telefonoConverter.toModels(requireNonNullElse(dto.getPhones(), List.of()));
            models.forEach(t -> {
                if (t.getId() == null) {
                    t.setCreated(DateUtil.fechaStgo());
                } else {
                    t.setModified(DateUtil.fechaStgo());
                }
            });
            model.setPhones(models);
            model.setModified(DateUtil.fechaStgo());
            model.setLastLogin(DateUtil.fechaStgo());
            model.setActive(dto.isActive());
            usuarioRepository.saveAndFlush(model);
        } else {
            throw new MSException(CatalogoEnum.ERROR_ACTUALIZAR_NO_EXISTE);
        }
    }

    @Loggear(clase = UsuarioService.class, anterior = INICIO, posterior = FIN)
    public void deleteAll() {
        usuarioRepository.deleteAll();
    }

    @Loggear(clase = UsuarioService.class, anterior = INICIO, posterior = FIN)
    public void deleteById(Integer id) {
        usuarioRepository.deleteById(id);
    }
}
