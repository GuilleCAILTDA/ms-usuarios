package cl.guille.usuarios.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.List;
import java.util.Map;

import static cl.guille.usuarios.enums.LogEstadoEnum.WARN;

public class JsonUtil {
    private static final Logger LOGGER = LogManager.getLogger(JsonUtil.class);
    public static final ObjectMapper MAPPER;

    private JsonUtil() {
        throw new UnsupportedOperationException();
    }

    static {
        MAPPER = (new ObjectMapper())
                .setSerializationInclusion(JsonInclude.Include.NON_NULL)
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    public static <T> String toJson(T object) {
        try {
            return MAPPER.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            LogUtil.logDebug(LOGGER, "toJson", WARN.toString(), null, e);
            return null;
        }
    }

    public static <T> String toJson(T object, boolean encoded) {
        if (encoded) {
            return Base64
                    .getEncoder()
                    .encodeToString(toJson(object).getBytes(StandardCharsets.UTF_8));
        } else {
            return toJson(object);
        }
    }

    public static <T> T toObject(String json, Class<T> clase) {
        try {
            return MAPPER.readValue(json, clase);
        } catch (JsonProcessingException e) {
            LogUtil.logDebug(LOGGER, "toObject", WARN.toString(), null, e);
            return null;
        }
    }

    public static <T> T toObject(String json, boolean decoded, Class<T> clase) {
        if (decoded) {
            return toObject(new String(Base64.getDecoder().decode(json.getBytes(StandardCharsets.UTF_8))
                            , StandardCharsets.UTF_8),
                    clase);
        }
        return toObject(json, clase);
    }

    public static <T> T toObject(File json, Class<T> clase) {
        try {
            return MAPPER.readValue(json, clase);
        } catch (IOException e) {
            LogUtil.logDebug(LOGGER, "toObject", WARN.toString(), null, e);
            return null;
        }
    }

    public static <T> T toListObject(String json, Class<?> clase) {
        try {
            return MAPPER.readValue(json, listOf(clase));
        } catch (JsonProcessingException e) {
            LogUtil.logDebug(LOGGER, "toListObject", WARN.toString(), null, e);
            return null;
        }
    }

    public static <T> T toMapObject(String json, Class<?> keyClase, Class<?> valueClase) {
        try {
            return MAPPER.readValue(json, mapOf(keyClase, valueClase));
        } catch (JsonProcessingException e) {
            LogUtil.logDebug(LOGGER, "toMapObject", WARN.toString(), null, e);
            return null;
        }
    }

    public static <T> T convertToListObject(Object object, Class<?> clase) {
        return MAPPER.convertValue(object, listOf(clase));
    }

    private static <T> JavaType listOf(Class<?> clase) {
        return TypeFactory.defaultInstance().constructCollectionType(List.class, clase);
    }

    private static <K, V> JavaType mapOf(Class<?> keyClase, Class<?> valueClase) {
        return TypeFactory.defaultInstance().constructMapType(Map.class, keyClase, valueClase);
    }
}
