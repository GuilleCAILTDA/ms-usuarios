package cl.guille.usuarios.utils;

import org.springframework.http.HttpHeaders;

public class HeadersUtil {

    private HeadersUtil() {
        throw new UnsupportedOperationException();
    }

    public final static HttpHeaders getGenericHeaders(String jwt) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN, "*");
        headers.add(HttpHeaders.ACCESS_CONTROL_ALLOW_METHODS, "*");
        headers.add(HttpHeaders.ACCESS_CONTROL_MAX_AGE, "3600");
        headers.add(HttpHeaders.ACCESS_CONTROL_ALLOW_CREDENTIALS, "false");
        headers.add(HttpHeaders.ACCESS_CONTROL_ALLOW_HEADERS, "*");
        headers.add("X-Frame-Options", "deny");
        headers.add("X-XSS-Protection", "1; mode=block");
        headers.add("Strict-Transport-Security", "max-age=31536000; includeSubDomains");
        headers.add(HttpHeaders.AUTHORIZATION, jwt);
        return headers;
    }
}
