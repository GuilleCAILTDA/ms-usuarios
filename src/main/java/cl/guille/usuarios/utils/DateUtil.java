package cl.guille.usuarios.utils;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

public class DateUtil {

    private DateUtil() {
        throw new UnsupportedOperationException();
    }

    public static ZonedDateTime timeZoneStgo() {
        LocalDateTime now = LocalDateTime.now();
        ZonedDateTime time = now.atZone(ZoneId.of("UTC"));
        return time.withZoneSameInstant(ZoneId.of("America/Santiago"));
    }

    public static Date fechaStgo() {
        return Date.from(timeZoneStgo().toInstant());
    }
}
