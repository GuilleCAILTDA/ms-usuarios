package cl.guille.usuarios.utils;

import cl.guille.usuarios.exceptions.Error;
import org.apache.logging.log4j.Logger;

import static java.util.Objects.requireNonNullElse;

public class LogUtil {

    private LogUtil() {
        throw new UnsupportedOperationException();
    }

    public static void logDebug(Logger LOGGER, String metodo, String log, String detalle, Exception e) {
        Error error = null;
        if (e != null) {
            Throwable throwable = requireNonNullElse(e.getCause(), e);
            error = Error.builder()
                    .traza(throwable.getLocalizedMessage())
                    .causa(throwable.getClass().getSimpleName())
                    .build();
        }

        LOGGER.error("metodo[{}] | log[{}] | detalle[{}] | throw[{}]",
                metodo,
                log,
                detalle,
                requireNonNullElse(error, Error.builder().build()));
    }

    public static void logInfo(Logger LOGGER, String metodo, String log, String detalle) {
        LOGGER.info("metodo[{}] | log[{}] | detalle[{}]",
                metodo,
                log,
                detalle);
    }

    public static void logError(Logger LOGGER, String metodo, String log, String detalle, Exception e) {
        Throwable throwable = requireNonNullElse(e.getCause(), e);
        Error error = Error.builder()
                .traza(throwable.getLocalizedMessage())
                .causa(throwable.getClass().getSimpleName())
                .build();

        LOGGER.error("metodo[{}] | log[{}] | detalle[{}] | throw[{}]",
                metodo,
                log,
                detalle,
                error);
    }
}