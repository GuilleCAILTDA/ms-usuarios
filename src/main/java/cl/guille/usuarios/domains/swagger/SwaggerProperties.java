package cl.guille.usuarios.domains.swagger;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Delegate;

@Getter
@Setter
public class SwaggerProperties {

    @Delegate
    private SwaggerUiEnabled ui;

    @Delegate
    private SwaggerInfo info;

    @Getter
    @Setter
    private static class SwaggerUiEnabled {
        private boolean enabled;
    }

    @Getter
    @Setter
    private static class SwaggerInfo {
        private String name;
        private String description;
        private String version;
        private SwaggerInfoContact contact;
        private boolean enable;
    }
}
