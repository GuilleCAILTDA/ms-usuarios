package cl.guille.usuarios.domains.swagger;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SwaggerInfoContact {
    private String name;
    private String mail;
}