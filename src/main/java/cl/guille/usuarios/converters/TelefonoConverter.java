package cl.guille.usuarios.converters;

import cl.guille.usuarios.daos.models.TelefonoModel;
import cl.guille.usuarios.dtos.TelefonoDTO;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class TelefonoConverter implements Converter<TelefonoDTO, TelefonoModel> {

    public TelefonoModel convert(TelefonoDTO dto) {
        return TelefonoModel
                .builder()
                .id(dto.getId())
                .number(dto.getNumber())
                .cityCode(dto.getCityCode())
                .countryCode(dto.getCountryCode())
                .created(dto.getCreated())
                .modified(dto.getModified())
                .build();
    }

    public TelefonoDTO convert(TelefonoModel model) {
        return TelefonoDTO
                .builder()
                .id(model.getId())
                .number(model.getNumber())
                .cityCode(model.getCityCode())
                .countryCode(model.getCountryCode())
                .created(model.getCreated())
                .modified(model.getModified())
                .build();
    }

    public List<TelefonoModel> toModels(List<TelefonoDTO> dtos) {
        List<TelefonoModel> models = new ArrayList<>();
        for (TelefonoDTO dto : dtos) {
            models.add(convert(dto));
        }
        return models;
    }

    public List<TelefonoDTO> toDTOs(List<TelefonoModel> models) {
        List<TelefonoDTO> dtos = new ArrayList<>();
        for (TelefonoModel model : models) {
            dtos.add(convert(model));
        }
        return dtos;
    }
}
