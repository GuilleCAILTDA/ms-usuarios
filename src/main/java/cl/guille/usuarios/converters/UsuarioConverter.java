package cl.guille.usuarios.converters;

import cl.guille.usuarios.daos.models.UsuarioModel;
import cl.guille.usuarios.dtos.UsuarioDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import static java.util.Objects.requireNonNullElse;

@Component
public class UsuarioConverter implements Converter<UsuarioDTO, UsuarioModel> {

    @Autowired
    private TelefonoConverter telefonoConverter;

    public UsuarioModel convert(UsuarioDTO dto) {
        return UsuarioModel
                .builder()
                .id(dto.getId())
                .name(dto.getName())
                .email(dto.getEmail())
                .password(dto.getPassword())
                .phones(telefonoConverter.toModels(requireNonNullElse(dto.getPhones(), List.of())))
                .created(dto.getCreated())
                .modified(dto.getModified())
                .lastLogin(dto.getLastLogin())
                .token(dto.getToken())
                .isActive(dto.isActive())
                .build();
    }

    public UsuarioDTO convert(UsuarioModel model) {
        return UsuarioDTO
                .builder()
                .id(model.getId())
                .name(model.getName())
                .email(model.getEmail())
                .password(model.getPassword())
                .phones(telefonoConverter.toDTOs(requireNonNullElse(model.getPhones(), List.of())))
                .created(model.getCreated())
                .modified(model.getModified())
                .lastLogin(model.getLastLogin())
                .token(model.getToken())
                .isActive(model.isActive())
                .build();
    }

    public List<UsuarioModel> toModels(List<UsuarioDTO> dtos) {
        List<UsuarioModel> models = new ArrayList<>();
        for (UsuarioDTO dto : dtos) {
            models.add(convert(dto));
        }
        return models;
    }

    public List<UsuarioDTO> toDTOs(List<UsuarioModel> models) {
        List<UsuarioDTO> dtos = new ArrayList<>();
        for (UsuarioModel model : models) {
            dtos.add(convert(model));
        }
        return dtos;
    }
}
