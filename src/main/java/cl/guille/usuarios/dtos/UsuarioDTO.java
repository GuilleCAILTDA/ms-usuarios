package cl.guille.usuarios.dtos;

import cl.guille.usuarios.validators.Password;
import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import java.util.Date;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class UsuarioDTO {
    private Integer id;
    private String name;
    @NotEmpty
    @Pattern(
            regexp = "^([a-z0-9_\\.-]+)@([0-9a-z\\.-]+)\\.([a-z\\.]{2,6})$"
    )
    private String email;
    @NotEmpty
    @Password
    private String password;
    private List<TelefonoDTO> phones;
    private Date created;
    private Date modified;
    @JsonAlias({"last_login", "lastLogin"})
    private Date lastLogin;
    private String token;
    @JsonAlias({"isactive", "isActive"})
    private boolean isActive;
}
