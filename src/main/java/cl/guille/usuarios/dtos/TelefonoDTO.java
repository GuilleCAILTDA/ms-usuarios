package cl.guille.usuarios.dtos;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class TelefonoDTO {
    private Integer id;
    private String number;
    @JsonAlias({"citycode", "cityCode"})
    private String cityCode;
    @JsonAlias({"contrycode", "countryCode"})
    private String countryCode;
    private Date created;
    private Date modified;
}
