# ms-usuarios

compilar aplicacion:
mvn clean install

iniciar aplicacion:
mvn spring-boot:run

La aplicacion no necesita nada para ser levantada, se encuentra con test asi que
los cambios en el formato de la password podrian afectarlos, favor considerar la actualizacion
de los jsons de prueba.

Por tiempo no se alcanzo a implementar el jwt, pero si se dejo la recepcion en la cabecera

Dejo los curls de prueba en caso de ser necesarios.

- swagger
localhost:8080/swagger-ui.html

- guardado de un usuario
  curl --location --request POST 'localhost:8080/usuario' \
  --header 'Authorization: jwt' \
  --header 'Content-Type: application/json' \
  --data-raw '{
  "name": "Juan Rodriguez",
  "email": "juan@rodriguez.org",
  "password": "hunter1",
  "phones": [
  {
  "number": "1234567",
  "cityCode": "1",
  "countryCode": "57"
  }
  ],
  "token": "jwt"
  }'

- obtencion de todos los usuarios
curl --location --request GET 'localhost:8080/usuario' \
--header 'Authorization: jwt'

- obtener un usuario
  curl --location --request GET 'localhost:8080/usuario/1' \
  --header 'Authorization: jwt'

- actualizar un usuario
  curl --location --request PUT 'localhost:8080/usuario/1' \
  --header 'Authorization: jwt' \
  --header 'Content-Type: application/json' \
  --data-raw '{
  "name": "Juan Pablo Rodriguez",
  "email": "juan@rodriguez.org",
  "password": "hunter2",
  "phones": [
  {
  "number": "1234567",
  "cityCode": "2",
  "countryCode": "58"
  }
  ],
  "token": "jwt"
  }'

- Eliminar un usuario
  curl --location --request DELETE 'localhost:8080/usuario/1' \
  --header 'Authorization: jwt'

- Eliminar todos los usuarios
  curl --location --request DELETE 'localhost:8080/usuario' \
  --header 'Authorization: jwt'